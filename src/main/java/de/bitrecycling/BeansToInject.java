package de.bitrecycling;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansToInject {
    
    @Bean
    public MyType lombokBeanToUse(){
        return new MyType();
    }

    @Bean
    public MyType autowiredBeanToUse(){
        return new MyType();
    }
    
}
