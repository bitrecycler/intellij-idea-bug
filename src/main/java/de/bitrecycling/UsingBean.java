package de.bitrecycling;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UsingBean {
    
    private final MyType lombokBeanToUse;
    @Autowired
    private MyType autowiredBeanToUse;

    public void useBean(){
        lombokBeanToUse.doStuff(); //this is the place that is navigated to
        autowiredBeanToUse.doStuff(); //this is the place that is navigated to
    }
}
